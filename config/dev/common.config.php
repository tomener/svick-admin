<?php
return [
    'project' => [
        'mode' => 0, //0单模块 1多模块 2多版本
    ],
    'http_content_type' => 'json',
    'cache' => [
        'type' => 'File',
        'sub_dir' => 'cache',
        'use_random_dir' => true,
        'path_level' => 2,
    ],
];
