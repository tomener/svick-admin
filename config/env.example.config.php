<?php

return [
    'env' => 'dev',
    'app_debug' => true,
    'pwd_salt' => 'C29D3l',
    'encryption' => [
        'aes' => [
            'key' => '42zmp2DD474E70Za',
            'method' => 'AES-128-CBC'
        ],
    ],
];
