## Svick Framework

一个简单、快速、规范、灵活、扩展性好的php http api开发框架。

## 🚀 特点
- 1、PHP7.2+
- 2、PSR-4标准自动加载
- 3、轻量级，扩展灵活，快速上手
- 4、支持服务容器，实现依赖注入、控制反转
- 5、支持单应用、多应用、多版本API
- 6、多环境支持，如开发环境（dev）测试环境（test）线上环境（pro）
- 7、支持cli模式运行
- 8、ORM链式调用，支持数据库读写分离设置，可具体到某张表
- 9、简单好用的Http客户端
    
## 🦊 安装
首先进入您存放项目的目录，如：cd D:\php
```
> composer create-project tomener/svick svick-demo

Do you want to remove the existing VCS (.git, .svn..) history? [Y,n]?
> Y

> cd svick-demo

> php svick serve
```
## 👉 访问
> 地址1：http://localhost:8090
>
> 地址2：http://your ip:8090

## 📕 文档
地址：http://svick.gumaor.com/

## 当然也可以使用apache、nginx

### apache
```
<VirtualHost *:9000>
    ServerAdmin webmaster@svick.com
    DocumentRoot "D:\php\svick-demo\public"
    ServerName localhost
    ErrorLog "logs/api.svick-demo.de-error.log"
    CustomLog "logs/api.svick-demo.de-access.log" common

    <Directory "D:\php\svick-demo\public">
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
        Header set Access-Control-Allow-Origin *
        Header set Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept, Token"
    </Directory>
</VirtualHost>
```

### nginx
```
server {
    listen       9000;
    server_name  localhost

    charset utf-8;
    access_log off;

    root /data/svick-demo/public;

    error_page 404 /404.html;

    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Headers 'Token,Uptoken';
    add_header Access-Control-Allow-Methods GET,POST,OPTIONS;
    add_header Access-Control-Max-Age 86400;

    location / {
        if ($request_method = 'OPTIONS') {
            return 204;
        }
        index index.php index.html;
        if (!-e $request_filename) {
            rewrite ^/(.*)$ /index.php/$1 last;
        }
    }

    location ~ \.php(/|$) {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        fastcgi_pass   unix:/dev/shm/php-fpm.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  PATH_INFO        $fastcgi_path_info;
        include        fastcgi_params;
    }
}
```
