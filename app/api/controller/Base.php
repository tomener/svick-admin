<?php
/**
 * Base.php
 * User: tommy
 */

namespace app\api\controller;


use lib\token\TokenLib;
use Svick\Core\App;
use Svick\Core\Request;
use Svick\Http\Response;

class Base
{
    public static $u;

    public function __construct()
    {
        if (Request::method() == 'OPTIONS') {
            Response::default()->send('');
        }

        self::checkToken();
    }

    /**
     * 验证TOKEN有效性
     *
     * @return bool
     */
    private static function checkToken()
    {
        $token = Request::getHeaders('Token');
        if (is_null($token)) {
            Response::default()->send(App::result(100403, 'token invalid'));
        }

        list($code, $msg, $uid) = TokenLib::validate($token);
        if ($code == -1) {
            Response::default()->send(App::result(100403, 'token expire'));
        } elseif ($code != 0) {
            Response::default()->send(App::result(100403, 'token error'));
        }

        $uid = intval($uid);

        Base::$u = [
            'uid' => $uid
        ];

        return true;
    }
}
