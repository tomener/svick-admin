<?php
/**
 * Index.php
 * User: tommy
 */

namespace app\api\controller;


use Svick\Core\App;

class Index
{
    /**
     * @return array|string
     */
    public function index()
    {
        return App::result(0, 'Svick Version:' . App::$version . ' Environment:' . App::$env);
    }
}
