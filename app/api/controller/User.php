<?php
/**
 * User.php
 * User: tommy
 */

namespace app\api\controller;


use Svick\Core\App;
use Svick\Core\Request;
use model\user\User as UserModel;

class User
{
    /**
     * 用户列表
     *
     * @return array|string
     */
    public function list()
    {
        $p = Request::getInt('p', 1);

        $where = [
            'status' => 1
        ];
        $page = ['limit' => 10, 'p' => $p];
        $users = UserModel::where($where)
            ->select('id, name, age')
            ->orderBy('id DESC')
            ->page($page)
            ->list();

        return App::result(0, 'ok', [
            'users' => $users,
            'page' => $page
        ]);
    }

    /**
     * 用户信息
     *
     * @return array|string
     */
    public function info()
    {
        $id = Request::getInt('id');

        if ($id <= 0) {
            return App::result(1, 'params error');
        }

        $user = UserModel::find($id, 'id, name, age');

        return App::result(0, 'ok', [
            'user' => $user
        ]);
    }
}
