<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\qiniu;


use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use function Qiniu\base64_urlSafeEncode;
use Qiniu\Processing\PersistentFop;
use Qiniu\Storage\BucketManager;
use Svick\Config\Config;


class QiniuLib
{
    /**
     * @var Auth 鉴权对象
     */
    protected static $auth;

    protected static $domains;

    /**
     * 获取文件状态
     *
     * @param $bucket
     * @param $key
     * @return array
     */
    public static function getFileStat($bucket, $key)
    {
        //初始化签权对象
        $auth = self::getAuth();

        //初始化BucketManager
        $bucketMgr = new BucketManager($auth);

        $bucket = QiniuLib::getBucket($bucket);

        return $bucketMgr->stat($bucket, $key);
    }

    /**
     * 上传文件
     * 如果key已存在，将上传失败{"code":614,"message":"file exists"}
     *
     * @param $filepath
     * @param $bucket
     * @param $key
     * @return array
     * @throws \Exception
     */
    public static function uploadFile($filepath, $bucket, $key)
    {
        //初始化签权对象
        $auth = self::getAuth();
        $bucket = self::getBucket($bucket);
        $token = $auth->uploadToken($bucket);
        $uploadMgr = new UploadManager();

        return $uploadMgr->putFile($token, $key, $filepath);
    }

    /**
     * 上传（字节流）
     * 如果key已存在，将上传失败{"code":614,"message":"file exists"}
     *
     * @param $stream
     * @param $bucket
     * @param null $key
     * @return array
     * @throws \Exception
     */
    public static function upload($stream, $bucket, $key = null)
    {
        //初始化签权对象
        $auth = self::getAuth();
        $bucket = self::getBucket($bucket);
        $token = $auth->uploadToken($bucket);
        $uploadMgr = new UploadManager();

        return $uploadMgr->put($token, $key, $stream);
    }

    /**
     * 移动临时文件
     *
     * @param $key1
     * @param $bucket2
     * @param null $key2
     * @return array [$ret, $err]
     */
    public static function moveTmpFile($key1, $bucket2, $key2 = null)
    {
        if (is_null($key2)) {
            $key2 = $key1;
        }
        return self::moveQiNiuFile('tmp', $key1, $bucket2, $key2);
    }

    /**
     * 移动文件
     *
     * @param $bucket1
     * @param $key1
     * @param $bucket2
     * @param $key2
     * @return array
     */
    public static function moveQiNiuFile($bucket1, $key1, $bucket2, $key2)
    {
        $bucket1 = self::getBucket($bucket1);
        $bucket2 = self::getBucket($bucket2);

        //初始化签权对象
        $auth = self::getAuth();

        //初始化BucketManager
        $bucketMgr = new BucketManager($auth);

        return $bucketMgr->move($bucket1, $key1, $bucket2, $key2);
    }

    /**
     * 复制文件
     *
     * @param $bucket1
     * @param $key1
     * @param $bucket2
     * @param $key2
     * @return mixed
     */
    public static function copyFile($bucket1, $key1, $bucket2, $key2)
    {
        $bucket1 = self::getBucket($bucket1);
        $bucket2 = self::getBucket($bucket2);

        //初始化签权对象
        $auth = self::getAuth();

        //初始化BucketManager
        $bucketMgr = new BucketManager($auth);

        $err = $bucketMgr->copy($bucket1, $key1, $bucket2, $key2);
        return $err;
    }

    /**
     * 组装七牛地址
     *
     * @param $domain
     * @param $key
     * @return string
     */
    public static function buildQiniuUrl($domain, $key)
    {
        if (!self::$domains) {
            self::$domains = Config::runtime('qiniu.domain');
        }
        return self::$domains[$domain] . $key;
    }

    /**
     * 抓取网络图片
     *
     * @param $bucket
     * @param $url
     * @param $key
     * @return bool
     * @throws \Exception
     */
    public static function fetch($bucket, $url, $key)
    {
        //构建鉴权对象
        $auth = QiniuLib::getAuth();

        //要上传的空间
        $bucket = QiniuLib::getBucket($bucket);
        $bucketManager = new BucketManager($auth);

        //指定抓取的文件保存名称
        list(, $err) = $bucketManager->fetch($url, $bucket, $key);
        if ($err !== null) {
            return false;
        }
        return true;
    }

    /**
     * 删除文件
     *
     * @param $bucket
     * @param $key
     * @return mixed
     */
    public static function deleteQiNiuFile($bucket, $key)
    {
        //初始化签权对象
        $auth = self::getAuth();

        //初始化BucketManager
        $bucketMgr = new BucketManager($auth);

        $err = $bucketMgr->delete(self::getBucket($bucket), $key);
        return $err;
    }

    /**
     * 处理音频文件
     *
     * 目前只压缩了32k，以后可以压缩到40k、64k
     *
     * @param $bucket
     * @param $key
     * @return bool
     */
    public static function dealAudio($bucket, $key)
    {
        $src_key = 'src/' . $key;
        $err = self::moveQiNiuFile('tmp', $key, $bucket, $src_key);
        if ($err) {
            return false;
        }

        $key_32k = '32/' . $key;
        list($id, $err) = self::compressMp3File($bucket, $src_key, $bucket, $key_32k);
        if ($err) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 删除音频
     *
     * @param $bucket
     * @param $audio
     * @return bool
     */
    public static function deleteAudio($bucket, $audio)
    {
        //612是no such file or directory
        $key_32k = '32/' . $audio;
        $err = QiniuLib::deleteQiNiuFile($bucket, $key_32k);
        if ($err && $err->code() != 612) {
            return false;
        }

        $src_key = 'src/' . $audio;
        $err = QiniuLib::deleteQiNiuFile($bucket, $src_key);
        if ($err && $err->code() != 612) {
            return false;
        }

        return true;
    }

    /**
     * 压缩七牛上的Mp3文件
     *
     * @param string $bucket 要压缩文件所在的bucket
     * @param string $src_key 要压缩的文件
     * @param string $save_bucket 压缩过后的文件存放的bucket
     * @param string $save_key 压缩过后的文件
     * @return array
     */
    public static function compressMp3File($bucket, $src_key, $save_bucket, $save_key)
    {
        $bucket = self::getBucket($bucket);
        $save_bucket = self::getBucket($save_bucket);

        //初始化签权对象
        $auth = self::getAuth();

        //转码是使用的队列名称
        $pipeline = 'audio-compress';
        $fop = new PersistentFop($auth, $bucket, $pipeline);

        //要进行转码的转码操作
        $fops = "avthumb/mp3/aq/8/ar/24000";
        //$fops = "avthumb/mp3/ab/32k/ar/24000";
        $save_key = base64_urlSafeEncode($save_bucket . ':' . $save_key);

        $fops .= '|saveas/' . $save_key;

        list($id, $err) = $fop->execute($src_key, $fops);

        return [$id, $err];
    }

    /**
     * 获取上传token
     *
     * @return string
     */
    public static function getUpToken($bucket_name)
    {
        //初始化签权对象
        $auth = self::getAuth();

        $bucket = self::getBucket($bucket_name);
        $upToken = $auth->uploadToken($bucket);
        return $upToken;
    }

    /**
     * 获取七牛空间名称
     *
     * @param $bucket
     * @return mixed
     * @throws \Exception
     */
    public static function getBucket($bucket)
    {
        $bucket_conf = Config::runtime('qiniu.bucket');
        if (!isset($bucket_conf[$bucket])) {
            throw new \Exception('qiniu bucket ' . $bucket . ' no exists');
        }
        return $bucket_conf[$bucket];
    }

    /**
     * 获取资源头
     *
     * @param $domain
     * @return mixed
     * @throws \Exception
     */
    public static function getDomain($domain)
    {
        if (!self::$domains) {
            self::$domains = Config::runtime('qiniu.domain');
        }

        if (!isset(self::$domains[$domain])) {
            throw new \Exception('qiniu domain ' . $domain . ' no exists');
        }
        return self::$domains[$domain];
    }

    private static function getKey($type = 'ak')
    {
        $key_conf = Config::runtime('qiniu.key');
        return $key_conf[$type];
    }

    /**
     * 初始化签权对象
     *
     * @return Auth
     */
    public static function getAuth()
    {
        if (!self::$auth) {
            self::$auth = new Auth(self::getKey('ak'), self::getKey('sk'));
        }
        return self::$auth;
    }
}
