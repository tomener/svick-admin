<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\token;


use lib\encrypt\AesEncrypt;

class TokenLib
{
    public static function create($data, $expire)
    {
        $token = AesEncrypt::make()->expire($expire)->encrypt($data);
        return $token;
    }

    public static function validate($token)
    {
        $ret = AesEncrypt::make()->decrypt($token);
        return $ret;
    }
}
