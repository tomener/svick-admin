<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\format;


class Format
{
    /**
     * 格式化为存储格式（单位分）
     *
     * @param $price
     * @return int
     */
    public static function storePrice($price)
    {
        return intval(bcmul(floatval($price), 100));
    }

    /**
     * 格式化为显示格式（单位元）
     *
     * @param $price
     * @return float|int
     */
    public static function showPrice($price)
    {
        return $price / 100;
    }

    /**
     * 格式化为key value的hash模式
     *
     * @param $key_name
     * @param $value_name
     * @param $data
     * @return array
     */
    public static function hash($key_name, $value_name, &$data)
    {
        $hash = [];
        if (empty($data)) {
            return $hash;
        }
        foreach ($data as $row) {
            if (!empty($value_name)) {
                $hash[$row[$key_name]] = $row[$value_name];
            } else {
                $key = $row[$key_name];
                unset($row[$key_name]);
                $hash[$key] = $row;
            }
        }
        return $hash;
    }

    public static function now()
    {
        return date('Y-m-d H:i:s');
    }

    public static function date()
    {
        return date('Y-m-d');
    }

    /**
     * 格式化简短显示日期时间
     *
     * @param $datetime
     * @return string
     */
    public static function dateShow($datetime)
    {
        return $datetime > 0 ? date('Y-m-d H:i', $datetime) : '';
    }

    /**
     * 格式化显示完整日期时间
     *
     * @param $datetime
     * @return string
     */
    public static function datetimeShow($datetime)
    {
        return $datetime > 0 ? date('Y-m-d H:i:s', $datetime) : '';
    }

    /**
     * 格式化文件大小
     *
     * @param $size
     * @return string
     */
    public static function fileSize($size)
    {
        $res = '';
        if ($size < 1024) {
            $res = round($size, 2) . 'B';
        } else if ($size < 1048576) {
            $res = round($size / 1024, 2) . 'K';
        } else if ($size < 1073741824) {
            $res = round($size / 1048576, 2) . 'M';
        } else {
            $res = round($size / 1073741824, 2) . 'G';
        }
        return $res;
    }

    /**
     * 格式化星期
     *
     * @param $time
     * @return mixed|string
     */
    public static function weekday($time)
    {
        if (is_numeric($time)) {
            $weekday = array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
            return $weekday[date('w', $time)];
        }
        return '';
    }

    /**
     * 时间可读性
     *
     * @param $timestamp
     * @return string
     */
    public static function readable($timestamp)
    {
        $diff_time = NOW_TIME - $timestamp;
        $days = intval($diff_time / 86400);
        $time_text = $days . '天前';
        if ($days == 0) {
            $hours = intval($diff_time / 3600);
            $time_text = $hours . '小时前';
            if ($hours == 0) {
                $minutes = intval($diff_time / 60);
                $time_text = $minutes . '分钟前';
                if ($minutes == 0) {
                    $time_text = $diff_time . '秒前';
                }
            }
        }
        return $time_text;
    }
}
